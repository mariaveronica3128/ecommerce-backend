// [SECTION] Dependencies and Modules
const auth = require('../auth')
const exp = require("express");
const User = require("../models/User.js")
const controller = require('./../controllers/users.js');

// destructive verify form auth
const {verify} = auth;

//[SECTION] Routing Component
const route = exp.Router(); 
 
//[SECTION] Routes-[POST]
route.post('/register', (req, res) => {
    let userDetails = req.body; 
  controller.registerUser(userDetails).then(outcome => {
     res.send(outcome);
  });
});

// LOGIN
route.post("/login", controller.loginUser);


// GET USER DETAILS (option 2)
route.get('/details', auth.verify, (req, res)=>{
  let userId = auth.decode(req.headers.authorization).id;
  controller.getProfile(userId).then(resultFromController => res.send(resultFromController))    
})


// ASSIGN USER AS ADMIN
route.put('/set-admin/:userId', auth.verify,(req,res) =>{
  let isAdmin = auth.decode(req.headers.authorization).isAdmin
isAdmin ?controller.setAdmin(req.params.userId).then(result => res.send(result)): res.send('unauthorized')
});


//retrieve All Users
route.get('/allusers', auth.verify, (req, res) => {
  let isAdmin = auth.decode(req.headers.authorization).isAdmin
  isAdmin ?controller.retrieveAllUsers(req.body).then(result => res.send(result)): res.send('unauthorized')
});



module.exports = route;