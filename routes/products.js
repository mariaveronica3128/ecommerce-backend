const auth = require('../auth')
const exp = require("express");
const { verifyAdmin } = require("../auth");
const controller = require('../controllers/products');
const { checkNonAdmin } = require("../auth");

// destructive verify form auth
const {verify} = auth;

//[SECTION] Routing Component
const route = exp.Router(); 

// Add Product 
route.post('/create', auth.verify,(req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    isAdmin ? controller.createproduct(req.body).then(outcome => {
        res.send(outcome)
    })
    : res.send('unauthorized access')
});

//retrieve All product
route.get('/allproducts', (req, res) => {
    controller.retrieveAllProduct(req.body).then(result => {
        res.send(result)
    });
});

//  [SECTION] Retrieve ALL Active Products
route.get('/ActiveProducts', auth.verify, (req, res) => {
    controller.getActiveProducts(req.body).then((outcome) => {
        res.send(outcome);
    });
});

//  [SECTION] Retrieve Single Products
route.get('/:id', (req, res) => {
    let productId = req.params.id;
    controller.getProduct(productId).then(outcome => {
        res.send(outcome);
    });
});

// [SECTION] Update product information [ADMIN]
route.put('/:productId', auth.verify,(req,res) =>{
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    isAdmin ? controller.updateProduct(req.params, req.body).then(result => {
        res.send(result)})
        :res.send('unauthorized access')
});

//[SECTION]FUNCTION Archive Product [ADMIN]
route.put('/archive/:productId', auth.verify,(req,res) =>{
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    isAdmin ? controller.archiveProducts(req.params, req.body).then(result => {
        res.send(result)
    })
    :res.send ('Unauthorized access')
})


//[SECTION]FUNCTION UNArchive Product [ADMIN]
route.put('/unarchive/:productId', auth.verify,(req,res) =>{
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    isAdmin ? controller.unarchiveProducts(req.params, req.body).then(result => {
        res.send(result)
    })
    :res.send ('Unauthorized access')
})

// // Product Delete by Admin Only [ADMIN]
route.delete('/:id', auth.verify, (req,res) => {
    let id = req.params.id; 
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    isAdmin ? controller.deleteProduct(id).then(result => {
        res.send (result);
     }):
     res.send ({message: 'unathorized'})
})

//[SECTION] Export Route System
module.exports = route;