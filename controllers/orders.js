const User = require('../models/User');
const Order = require('../models/Order')
const Product = require("../models/Product")
const auth = require('../auth')
const { findById } = require('../models/Product');
const res = require('express/lib/response');


// module.exports.orderCreate = async (user, data) => {
//     let Status = user.isAdmin;
//     let userId = user.id;
//     let orderProductId = data.productId;
//     let orderProductQuantity = data.quantity;
//     let price = data.price
   
//     if (Status === true) {
//       return "Unauthorized User";
//     }
//     let findProductById = await Product.findById(orderProductId)
//       .then((result) => {
//         return result;
//       })
//       .catch((err) => err.message);
  
//     let newOrder = new Order({
//       totalAmount: (findProductById.price * orderProductQuantity),
//       userId: userId,
//       productsPurchased: [
//         {
//           productId: findProductById.id,
//           quantity: orderProductQuantity,      
         
//         },
//       ],
//     });
   
//     let UpdateOrders = await newOrder.save() 
//     .then((orderCreated) => { 
//         return orderCreated;
//       });
    
//     if (UpdateOrders !== null) {     

//            let UserOrder = {
//         orders: [
//           {
//             orderID: UpdateOrders.id,
//           },
//         ]
//       };
  
//       return User.findByIdAndUpdate(userId,{$push : UserOrder})
//         .then((result) => {
          
//           return (result)
//         })
//         .catch((err) => err.message);

//     }
// };

// Retrieve All ORDERS
module.exports.getAllOrder = (userData) => {
    
  return User.findById(userData.id).then(result => {
      if (userData.isAdmin) {
          return Order.find({}).then(result => {
              return result
          })
      } else {
          return Promise.reject('Not authorized to access this page');
      }
  })
}

module.exports.getAllMyOrder = (userData) => {
    
  return User.findById(userData.id).then(result => {
      if (userData.isAdmin) {
          return Promise.reject('Not authorized to access this page');
          
      } else {

          return Order.find({userId: userData.id}).then(result => {
              return result
          })
          
      }
  })
}


module.exports.checkout = (data) => {
	const {userId, productId, quantity} = data

	return User.findById(userId).then((result, err) => {
		if(err) {
			return false
		} else {
			return Product.findById(productId).then((result, err) => {
				let newOrder = new Order({
					userId: userId,
					totalAmount: quantity * result.price,
					productsPurchased: [{
						productId: productId,
						price: result.price,
						quantity: quantity
					}]
				})
				return newOrder.save().then((result, err) => {
					if(err) {
						return false
					} else { 
						return result 
					}
				})
			})
		}
	})
}





   






   
  
  
	





