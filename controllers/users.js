// [SECTION] Dependencies and Modules
const auth = require('../auth')
const User = require('../models/User');
const bcrypt = require("bcrypt");
const dotenv = require("dotenv")


//[SECTION] Environment Setup
dotenv.config();
const salt = Number(process.env.SALT); 

//[SECTION] Functionalities [CREATE]
module.exports.registerUser = (data) => {

    let email = data.email;
    let passW= data.password; 

    let newUser = new User({
        email: email,
        password: bcrypt.hashSync(passW, salt), 
    }); 
    return User.findOne({email: email}).then((userFound, error) => {
        if (userFound){
            return ({message: "Email exists. Use another email to register"})
        } 
        else {

     return newUser.save().then((user, rejected) => {
        if (user) {
            return true;         
        } else {
            return false
        }
    });
    }
    
  })

};


//Retrieve All 
module.exports.retrieveAllUsers = (prodObj) => {
    return User.find({}).then(result => {	
    return result		
    });
};

// LOGIN
module.exports.loginUser = (req, res) => {
    console.log(req.body)
    
    User.findOne({email: req.body.email}).then(foundUser => {
        if (foundUser === null){
            return res.send({message:"User not found"})
        } else {

            const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
            console.log(isPasswordCorrect) 

            if(isPasswordCorrect){
                return res.send({accessToken: auth.createAccessToken(foundUser)})
            } else {
                return res.send({message:"Incorrect password"})
            }
        }
    }).catch(err => {
        res.send(err)
    })
}

// GET USER DETAILS
module.exports.getUserDetails = (req, res) => {
   console.log(req.user)

   User.findById(req.user.id)
   .then(result => res.send(result))
   .catch(err => res.send(err))
};

module.exports.getProfile = (userId) => {
    return User.findById(userId).then(result => {
        result.password = undefined 
        return result;
    })
}

// ASSIGN USER AS ADMIN
module.exports.setAdmin = (id, res) => {
	let newStatus = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(id, newStatus).then((result, err) => {
		if(result) {
			return {message: "User successfully assigned as Admin."}
		} else {
			return false;
		}
	})
};



