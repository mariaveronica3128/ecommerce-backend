//[SECTION] Dependencies and Modules
const Product = require('../models/Product');

//[SECTION] Functionality [CREATE]
module.exports.createproduct = (info) => {
    let cName = info.ProductName;
    let cDesc = info.description;
    let cCat = info.category;
    let cColor = info.color;
    let cPrice = info.price;
    let cRating = info.rating;
    let cStock = info.stock
    let newProduct = new Product({
        ProductName: cName,
        description: cDesc,
        category: cCat,
        color: cColor,
        price: cPrice,
        rating: cRating,
        stock: cStock
    }) 
       return newProduct.save().then((savedProduct, error) => {
           if (error) {
               return false;
           } else {
               return true; 
           }
       });
  };

  //[SECTION]FUNCTION Retrieve 
  
	//Retrieve All  Products
    module.exports.retrieveAllProduct = (prodObj) => {
        return Product.find({}).then(result => {	
        return result		
        });
    };

//   [SECTION] Retrieve ALL Active Products [RETRIVE]
  module.exports.getActiveProducts = () => {
      return Product.find({isActive: true}).then(resultOfQuery => {
          return resultOfQuery;
      });
  };
  //  [SECTION] Retrieve Single Products
  module.exports.getProduct = (id) => {
      return Product.findById(id).then(resultOfQuery => {
          return resultOfQuery;
      });
  };

  //[SECTION]FUNCTION Update
	//update a product information (admin)
    module.exports.updateProduct = (product, prodObj) => {
        let updatedProduct = {
            ProductName : prodObj.ProductName,
            description : prodObj.description,
            color: prodObj.color,
            price : prodObj.price,
        }
        let id = product.productId
        return Product.findByIdAndUpdate(id, updatedProduct).then((result, err) => {
                if (result) {
                    return true
                } else {
                    return false
                }
        });
        };


    //[SECTION]FUNCTION Archive Product

	module.exports.archiveProducts = (product, prodObj) => {
		let newStatus = {
			isActive : false
		}
	
			let id = product.productId
		return Product.findByIdAndUpdate(id, newStatus).then((result, err) =>{
			if (result) {
				return true
			} else {
				return false
			}
		})
	};


    //[SECTION]FUNCTION Archive Product

	module.exports.unarchiveProducts = (product, prodObj) => {
		let newStatus = {
			isActive : true
		}
	
			let id = product.productId
		return Product.findByIdAndUpdate(id, newStatus).then((result, err) =>{
			if (result) {
				return true
			} else {
				return false
			}
		})
	};



// [SECTION] Functionality [DELETE]
module.exports.deleteProduct = (id) => {
	return Product.findByIdAndRemove(id).then((removedProduct, err) => {
		if (removedProduct) {
			return true
		} else {
			return false
		};
	});
};





  