//[SECTION] Dependencies and Modules
const mongoose = require('mongoose'); 

//[SECTION] Blueprint Schema
const ordersSchema = new mongoose.Schema({

    userId: {
        type: String,
        required: [true, 'User Id is required']
    },
    totalAmount: {
        type: Number,
        default: 0
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    productsPurchased: [ 
        {
            productId: {
                type: String,
                required: [true, 'Product Id is required']
            },
            price: {
                type: Number,
                required: [true, 'Price is required']
                
            },
            quantity: {
                type: Number,
                default: 1
            },
        }
    ]
})


// [Section] Model

const Order = mongoose.model('Order', ordersSchema)
module.exports = Order;
